package com.metrics.stats.domain.service;

import com.metrics.stats.domain.MachineParameter;

import java.util.List;

public interface MachineParameterService {
    void insert(List<MachineParameter> machineList);
}
